// A1

let enterUserName = prompt('Enter your username:');
let enterPassword = prompt('Enter your password:');
let enterRole = prompt('Enter your role:');

	if(enterRole === 'admin') {
		console.log(alert('Welcome back to the class portal, admin!'));
	} else if (enterRole === 'teacher') {
		console.log(alert('Thank you for logging in, teacher!'));
	} else if(enterRole === 'rookie') {
		console.log(alert('Welcome to the class portal, student!'));
	} else {
		console.log(alert('Role out of range!'));
	}

// A2

let checkRole = prompt('Please enter your role.');
	if (checkRole !== 'rookie') {
		console.log(alert('<role!> You are not allowed to access this feature!'));
	} else if (checkRole === 'rookie') {
		console.log(checkAverage());
	}

function checkAverage(mathGrade, englishGrade, scienceGrade, entrepGrade) {
	
		let average = ((mathGrade + englishGrade + scienceGrade + entrepGrade) / 4);
		average = Math.round(average);

		if (average <= 74 ) {
			console.log('Hello, student, your average is ' + average + "." + 'The letter equivalent is F');
		} else if (average >= 75 && average <= 79) {
			console.log('Hello, student, your average is ' + average + "." + 'The letter equivalent is D');
		} else if (average >= 80 && average <= 84) {
			console.log('Hello, student, your average is ' + average + "." + 'The letter equivalent is C');
		} else if (average >= 85 && average <= 89) {
			console.log('Hello, student, your average is ' + average + "." + 'The letter equivalent is B');
		} else if (average >= 90 && average <= 95) {
			console.log('Hello, student, your average is ' + average + "." + 'The letter equivalent is A');
		} else if (average > 96) {
			console.log('Hello, student, your average is ' + average + "." + 'The letter equivalent is A+');
		}
}
checkAverage();
/*browser console
checkAverage(71, 70, 73, 74);
checkAverage(75, 75, 76, 78);
checkAverage(80, 81, 82, 78);
checkAverage(84, 85, 87, 88);
checkAverage(89, 90, 91, 90);
checkAverage(91, 96, 97, 95);*/